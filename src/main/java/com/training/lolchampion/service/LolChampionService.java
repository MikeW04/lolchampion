package com.training.lolchampion.service;

import com.training.lolchampion.entites.LolChampion;
import com.training.lolchampion.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LolChampionService {

    @Autowired
    LolChampionRepository lolChampionRepository;

    public List<com.training.lolchampion.entites.LolChampion> findAll(){

        return lolChampionRepository.findAll();
    }


    public LolChampion save(LolChampion lolChampion){
        return lolChampionRepository.save(lolChampion);
    }


}
